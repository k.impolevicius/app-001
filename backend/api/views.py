from django.shortcuts import render
from rest_framework import viewsets
from api.serializers import CountrySerializer
from api.models import Country

# Create your views here.


class CountryViewSet(viewsets.ModelViewSet):
    queryset = Country.objects.all()
    serializer_class = CountrySerializer
