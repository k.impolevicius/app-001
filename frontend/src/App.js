import React, { useEffect, useState } from "react";
import axios from "axios";

const App = () => {
  const [countries, setCountries] = useState([]);
  const [country, setCountry] = useState({ name: "Germany" });

  const handleLoad = async () => {
    const response = await axios.get("http://localhost:1337/Country/");
    setCountries(response.data);
    // console.log(countries);
  };

  const handleInsert = async () => {
    const response = await axios.post(
      "http://localhost:1337/Country/",
      country
    );
  };

  const handleChange = (event) => {
    event.preventDefault();
    setCountry({
      name: event.target.value,
    });
  };

  return (
    <>
      <input name="name" value={country.name} onChange={handleChange}></input>
      <button onClick={handleInsert}> insert </button>
      <button onClick={handleLoad}> Load </button>

      <ul>
        {countries.map((country) => (
          <li key={country.id}> {country.name}</li>
        ))}
      </ul>
    </>
  );
};

export default App;
